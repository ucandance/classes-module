<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleClassesCreateLocationsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'locations',
         'title_column' => 'name',
         'translatable' => false,
         'versionable' => true,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "location" => [
            "required" => true,
        ],
        "instructions",
    ];
}
