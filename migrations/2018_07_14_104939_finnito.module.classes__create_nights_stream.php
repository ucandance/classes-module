<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleClassesCreateNightsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'nights',
         'title_column' => 'class_date',
         'translatable' => false,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'class_date' => [
            'required' => true,
        ],
        "night_classes" => [
            "required" => true,
        ],
        "class_location",
        // "class_style",
        // "class_start",
        // "class_end",

    ];

}
