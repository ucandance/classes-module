<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleClassesCreateNightClassesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'night_classes',
        "namespace" => "repeater",
         'title_column' => 'class_start',
         'translatable' => false,
         'versionable' => false,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        // 'name' => [
        //     'required' => true,
        // ],
        // 'slug' => [
        //     'unique' => true,
        //     'required' => true,
        // ],
        "class_style",
        "class_start",
        "class_end",

    ];
}
