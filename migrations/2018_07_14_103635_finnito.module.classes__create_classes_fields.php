<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
// use Finnito\ClassesModule\Night\NightModel;
use Finnito\ClassesModule\ClassType\ClassTypeModel;

use Finnito\ClassesModule\ClassType\Multiple\ClassTypeOptions;
use Finnito\ClassesModule\ClassType\Multiple\ClassTypeLookupTableBuilder;
use Finnito\ClassesModule\ClassType\Multiple\ClassTypeValueTableBuilder;
use Finnito\ClassesModule\ClassType\Multiple\ClassTypeSelectedTableBuilder;

// Relationship Models
use Finnito\ClassesModule\Style\StyleModel;
use Finnito\ClassesModule\Location\LocationModel;
use Finnito\ClassesModule\NightClass\NightClassModel;


class FinnitoModuleClassesCreateClassesFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        // Shared Fields
        "name" => [
            "type" => "anomaly.field_type.text",
        ],
        "slug" => [
            "type" => "anomaly.field_type.slug",
            "config" => [
                "slugify" => "name",
                "type" => "-",
            ],
        ],

        // Class Location Fields
        // Has "name" and "slug" +
        "location" => [
            "type" => "anomaly.field_type.geocoder",
            "config" => [],
        ],
        "instructions" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [],
        ],

        // Class Style Fields
        // Has "name" and "slug" +
        "description" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [],
        ],

        // Night Fields (not repeater)
        'class_date' => [
            "type" => "anomaly.field_type.datetime",
            "config" => [
                "mode" => "date",
                // "date_format" => "Y-m-d",
            ],
        ],
        "class_location" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => LocationModel::class,
            ],
        ],
        "night_classes" => [
            "type" => "anomaly.field_type.repeater",
            "config" => [
                "related" => NightClassModel::class,
            ],
        ],

        // Fields for each class in a night (repeater)
        "class_style" => [
            "type" => "anomaly.field_type.relationship",
            "namespace" => "repeater",
            "config" => [
                "related"        => StyleModel::class,
                "mode"           => "dropdown",
            ]
        ],
        "class_start" => [
            "type" => "anomaly.field_type.datetime",
            "namespace" => "repeater",
            "config" => [
                "mode" => "time",
                // "time_format" => "h:i",
            ],
        ],
        "class_end" => [
            "type" => "anomaly.field_type.datetime",
            "namespace" =>"repeater",
            "config" => [
                "mode" => "time",
                // "time_format" => "h:i",
            ],
        ],

        // Deprecated fields
        // 'night_classes'    => [
        //     'type'   => 'anomaly.field_type.multiple',
        //     'config' => [
        //         'related' => ClassTypeModel::class,
        //         "mode" => "lookup",
        //         "handler" => ClassTypeOptions::class,
        //         "lookup_table" => ClassTypeLookupTableBuilder::class,
        //         "value_table" => ClassTypeValueTableBuilder::class,
        //         "selected_table" => ClassTypeSelectedTableBuilder::class,
        //     ],
        // ],
        // "year" => [
        //     "type" => "anomaly.field_type.integer",
        //     "config" => [
        //         "separator" => "",
        //     ],
        // ],
    ];

}
