<?php

return [
    'nights' => [
        'title' => 'Nights',
    ],
    'class_types' => [
        'title' => 'Class Types',
    ],
    'locations' => [
        'title' => 'Locations',
    ],
    'styles' => [
        'title' => 'Styles',
    ],
    'night_classes' => [
        'title' => 'Night classes',
    ],
];
