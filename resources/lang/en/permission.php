<?php

return [
    'nights' => [
        'name'   => 'Nights',
        'option' => [
            'read'   => 'Can read nights?',
            'write'  => 'Can create/edit nights?',
            'delete' => 'Can delete nights?',
        ],
    ],
    'class_types' => [
        'name'   => 'Class types',
        'option' => [
            'read'   => 'Can read class types?',
            'write'  => 'Can create/edit class types?',
            'delete' => 'Can delete class types?',
        ],
    ],
    'locations' => [
        'name'   => 'Locations',
        'option' => [
            'read'   => 'Can read locations?',
            'write'  => 'Can create/edit locations?',
            'delete' => 'Can delete locations?',
        ],
    ],
    'styles' => [
        'name'   => 'Styles',
        'option' => [
            'read'   => 'Can read styles?',
            'write'  => 'Can create/edit styles?',
            'delete' => 'Can delete styles?',
        ],
    ],
    'night_classes' => [
        'name'   => 'Night classes',
        'option' => [
            'read'   => 'Can read night classes?',
            'write'  => 'Can create/edit night classes?',
            'delete' => 'Can delete night classes?',
        ],
    ],
];
