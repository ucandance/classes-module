<?php

return [
    "meta_description" => [
        "name" => "Classes Meta Description",
        "instructions" => "What is displayed in Google search results and in links on social media.",
    ],
    "meta_image" => [
        "name" => "Classes Meta Image",
        "instructions" => "The image displayed in Google search results and in links on social media.",
    ],
];
