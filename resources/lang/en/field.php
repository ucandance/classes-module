<?php

return [
	"date.name" => "Class Date",
	"name.name" => "Name",
	"year.name" => "Year",
	"slug.name" => "Slug",
	"class_start.name" => "Start Time",
	"class_end.name" => "End Time",
	"night_classes.name" => "Classes",
	"class_style.name" => "Style",
	"class_date.name" => "Date",
	"class_location.name" => "Location",
	"description.name" => "Description",
	"map.name" => "Location",
	"instructions.name" => "Instructions",
	"location.name" => "Location",
];
