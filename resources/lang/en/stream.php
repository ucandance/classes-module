<?php

return [
    'nights' => [
        'name' => 'Nights',
    ],
    'class_types' => [
        'name' => 'Class types',
    ],
    'locations' => [
        'name' => 'Locations',
    ],
    'styles' => [
        'name' => 'Styles',
    ],
    'night_classes' => [
        'name' => 'Night classes',
    ],
];
