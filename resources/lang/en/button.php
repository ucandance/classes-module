<?php

return [
    'new_night' => 'New Night',
    'new_class_type' => 'New Class Type',
    "duplicate" => "Duplicate +1 Week",
    "duplicate_edit" => "Duplicate & Edit",
    'new_location' => 'New Location',
    'new_style' => 'New Style',
    'new_night_class' => 'New Night class',
];
