<?php

return [
    'page_view' => [
        'type'   => 'anomaly.field_type.select',
        'config' => [
            'options' => [
                'table' => 'finnito.module.classes::preferences.classes_view.option.table',
                'calendar' => 'finnito.module.classes::preferences.classes_view.option.calendar',
            ],
        ],
    ],
];
