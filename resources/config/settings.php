<?php

return [
    "meta_description" => [
        "type" => "anomaly.field_type.textarea",
        "config" => [
            "show_counter" => true,
            "max" => "160",
        ],
    ],
    "meta_image" => [
        "type" => "anomaly.field_type.file",
        "config" => [],
    ],
];
