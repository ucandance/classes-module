<?php

return [
    'nights' => [
        'read',
        'write',
        'delete',
    ],
    'class_types' => [
        'read',
        'write',
        'delete',
    ],
    'locations' => [
        'read',
        'write',
        'delete',
    ],
    'styles' => [
        'read',
        'write',
        'delete',
    ],
    'night_classes' => [
        'read',
        'write',
        'delete',
    ],
];
