function initNightsFormHelper() {
	console.log("Booting NightsFormHelper.js");
	var input = document.querySelector('input[data-field_name="name"]');
	var location = document.querySelector("input[name='location[address]']");
	input.addEventListener("input", function() {
		location.value = input.value + ", Christchurch, New Zealand";
	});
	// document.querySelector("div.year-field input").value = new Date().getFullYear();
	// document.querySelector("div.class_start-field input.flatpickr-input.input").value = "7:30pm";
	// document.querySelector("div.class_start-field input[type=hidden].form-control.flatpickr-input").value = "7:30";
}
window.addEventListener("load", initNightsFormHelper);
