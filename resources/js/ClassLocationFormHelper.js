function initLocationFormHelper() {
    document.querySelector("div.name-field input").addEventListener("keyup", function() {
        var locationField = document.querySelector("div.location-field input");
        locationField.value = this.value + ", Christchurch, New Zealand";

        var event = new Event("keyup");
        locationField.dispatchEvent(event);
    });
}
window.addEventListener("load", initLocationFormHelper);
