<?php namespace Finnito\ClassesModule\Style;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class StyleObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleObserver extends EntryObserver
{

}
