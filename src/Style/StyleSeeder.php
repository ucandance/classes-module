<?php namespace Finnito\ClassesModule\Style;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class StyleSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
