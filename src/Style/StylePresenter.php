<?php namespace Finnito\ClassesModule\Style;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class StylePresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StylePresenter extends EntryPresenter
{

}
