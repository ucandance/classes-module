<?php namespace Finnito\ClassesModule\Style\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class StyleRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface StyleRepositoryInterface extends EntryRepositoryInterface
{

}
