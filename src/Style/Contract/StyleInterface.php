<?php namespace Finnito\ClassesModule\Style\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class StyleInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface StyleInterface extends EntryInterface
{

}
