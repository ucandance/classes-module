<?php namespace Finnito\ClassesModule\Style;

use Finnito\ClassesModule\Style\Contract\StyleInterface;
use Anomaly\Streams\Platform\Model\Classes\ClassesStylesEntryModel;

/**
 * Class StyleModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleModel extends ClassesStylesEntryModel implements StyleInterface
{

}
