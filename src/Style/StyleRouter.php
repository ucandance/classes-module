<?php namespace Finnito\ClassesModule\Style;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class StyleRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleRouter extends EntryRouter
{

}
