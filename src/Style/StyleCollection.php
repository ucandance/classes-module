<?php namespace Finnito\ClassesModule\Style;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class StyleCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleCollection extends EntryCollection
{

}
