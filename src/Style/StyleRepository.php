<?php namespace Finnito\ClassesModule\Style;

use Finnito\ClassesModule\Style\Contract\StyleRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class StyleRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleRepository extends EntryRepository implements StyleRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var StyleModel
     */
    protected $model;

    /**
     * Create a new StyleRepository instance.
     *
     * @param StyleModel $model
     */
    public function __construct(StyleModel $model)
    {
        $this->model = $model;
    }
}
