<?php namespace Finnito\ClassesModule\Style;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class StyleCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StyleCriteria extends EntryCriteria
{

}
