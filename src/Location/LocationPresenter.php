<?php namespace Finnito\ClassesModule\Location;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class LocationPresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationPresenter extends EntryPresenter
{

}
