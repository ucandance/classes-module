<?php namespace Finnito\ClassesModule\Location\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class LocationInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface LocationInterface extends EntryInterface
{

}
