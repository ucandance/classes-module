<?php namespace Finnito\ClassesModule\Location\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class LocationRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface LocationRepositoryInterface extends EntryRepositoryInterface
{

}
