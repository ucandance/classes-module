<?php namespace Finnito\ClassesModule\Location;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class LocationObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationObserver extends EntryObserver
{

}
