<?php namespace Finnito\ClassesModule\Location;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class LocationSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
