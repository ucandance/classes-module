<?php namespace Finnito\ClassesModule\Location;

use Finnito\ClassesModule\Location\Contract\LocationRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class LocationRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationRepository extends EntryRepository implements LocationRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var LocationModel
     */
    protected $model;

    /**
     * Create a new LocationRepository instance.
     *
     * @param LocationModel $model
     */
    public function __construct(LocationModel $model)
    {
        $this->model = $model;
    }
}
