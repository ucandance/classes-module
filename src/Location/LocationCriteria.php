<?php namespace Finnito\ClassesModule\Location;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class LocationCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationCriteria extends EntryCriteria
{

}
