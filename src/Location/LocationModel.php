<?php namespace Finnito\ClassesModule\Location;

use Finnito\ClassesModule\Location\Contract\LocationInterface;
use Anomaly\Streams\Platform\Model\Classes\ClassesLocationsEntryModel;

/**
 * Class LocationModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationModel extends ClassesLocationsEntryModel implements LocationInterface
{

}
