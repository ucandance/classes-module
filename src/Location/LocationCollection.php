<?php namespace Finnito\ClassesModule\Location;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class LocationCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationCollection extends EntryCollection
{

}
