<?php namespace Finnito\ClassesModule\Location;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class LocationRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationRouter extends EntryRouter
{

}
