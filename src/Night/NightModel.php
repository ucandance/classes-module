<?php namespace Finnito\ClassesModule\Night;

use Anomaly\Streams\Platform\Model\Classes\ClassesNightsEntryModel;
use Finnito\ClassesModule\Night\Contract\NightInterface;

/**
 * Class NightModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightModel extends ClassesNightsEntryModel implements NightInterface
{

    /**
     * Get an HTML formatted string
     * of the classes for the night.
     *
     * @return null|string
     */
    public function getClassNames()
    {
        $classesArray = $this->night_classes;
        $classes = "<span>";
        for ($i = 0; $i < sizeof($classesArray); $i++) {
            $class = $classesArray[$i];
            $className = $class->class_style->name;
            $class_start = $class["class_start"]->format("H:i");
            $class_end = $class["class_end"]->format("H:i");
            $classes .= "<strong>{$className}</strong> ({$class_start} to {$class_end})<br>";
        }
        $classes .= "</span>";
        return $classes;
    }
}
