<?php namespace Finnito\ClassesModule\Night;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class NightCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightCriteria extends EntryCriteria
{

    /**
     * Return the next classe
     * for use in the header.
     *
     * @return NightModelInterface|null
     */
    public function nextClass()
    {
        return $this->query
            ->where("class_date", ">=", date("Y-m-d"))
            ->orderBy("class_date", "ASC")
            ->first();
    }
}
