<?php namespace Finnito\ClassesModule\Night\Support\MultipleFieldType;

/**
 * Class ValueTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ValueTableBuilder extends \Anomaly\MultipleFieldType\Table\ValueTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        'name',
        "id",
    ];
}
