<?php namespace Finnito\ClassesModule\Night\Support\MultipleFieldType;

/**
 * Class LookupTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LookupTableBuilder extends \Anomaly\MultipleFieldType\Table\LookupTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        'name',
        'start_time',
        "end_time",
    ];
}
