<?php namespace Finnito\ClassesModule\Night\Support\MultipleFieldType;

/**
 * Class SelectedTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class SelectedTableBuilder extends \Anomaly\MultipleFieldType\Table\SelectedTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        'name',
        'start_time',
        "end_time",
    ];
}
