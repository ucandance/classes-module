<?php namespace Finnito\ClassesModule\Night\Support\RelationshipFieldType;

/**
 * Class SelectedTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class SelectedTableBuilder extends \Anomaly\RelationshipFieldType\Table\SelectedTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        "class_date" => [
            'heading' => 'finnito.module.classes::message.class_date',
            'value'   => "entry.class_date.date|date('j M, \\'y')",
        ],
    ];
}
