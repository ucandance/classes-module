<?php namespace Finnito\ClassesModule\Night\Support\RelatedFieldType;

/**
 * Class LookupTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LookupTableBuilder extends \Anomaly\RelationshipFieldType\Table\LookupTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        "class_date" => [
            'heading' => 'finnito.module.classes::message.class_date',
            'value'   => "entry.class_date.date|date('j M, \\'y')",
        ],
        "classes" => [
            "heading" => "finnito.module.classes::message.classes",
            "value" => "entry.getClassNames()",
        ],
    ];

    protected $options = [
        'order_by' => [
            'class_date' => 'DESC',
        ],
    ];
}
