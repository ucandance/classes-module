<?php namespace Finnito\ClassesModule\Night\Support\RelatedFieldType;

/**
 * Class ValueTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ValueTableBuilder extends \Anomaly\RelationshipFieldType\Table\ValueTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        "class_date" => [
            'heading' => 'finnito.module.classes::message.class_date',
            'value'   => "entry.class_date.date|date('j M, \\'y')",
        ],
    ];
}
