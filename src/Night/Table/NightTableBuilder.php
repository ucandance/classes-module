<?php namespace Finnito\ClassesModule\Night\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class NightTableBuilder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [

    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        "class_date" => [
            'heading' => 'finnito.module.classes::message.class_date',
            'value'   => 'entry.class_date.date|date("D, j F, Y")',
        ],
        "classes" => [
            "heading" => "finnito.module.classes::message.classes",
            "value" => "entry.getClassNames()",
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
        'duplicate'          => [
            'url' => '/admin/classes/duplicate/{entry.id}',
            'text' => 'finnito.module.classes::button.duplicate',
            'icon' => 'glyphicons glyphicons-duplicate',
            'type' => 'success',
        ],
        'duplicate_edit'          => [
            'url' => '/admin/classes/duplicate_edit/{entry.id}',
            'text' => 'finnito.module.classes::button.duplicate_edit',
            'icon' => 'glyphicons glyphicons-duplicate',
            'type' => 'success',
        ],
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [
        'order_by' => [
            'class_date' => 'ASC',
        ],
    ];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];
}
