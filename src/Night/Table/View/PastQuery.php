<?php namespace Finnito\ClassesModule\Night\Table\View;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class PastQuery
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class PastQuery
{

    /**
     * Handle the view query
     *
     * @return Builder
     */
    public function handle(Builder $query)
    {
        $query->where("class_date", "<", date("Y-m-d"));
    }
}
