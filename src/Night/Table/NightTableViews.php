<?php namespace Finnito\ClassesModule\Night\Table;

use Finnito\ClassesModule\Night\Contract\NightRepositoryInterface;
use Finnito\ClassesModule\Night\Table\View\UpcomingQuery;
use Finnito\ClassesModule\Night\Table\View\PastQuery;

/**
 * Class NightTableViews
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightTableViews
{

    /**
     * The NightRepositoryInterface
     *
     * @var interface
     */
    protected $nights;

    /**
     * Create a new NightTableViews instance.
     *
     * @param NightRepositoryInterface $nights
     */
    public function __construct(NightRepositoryInterface $nights)
    {
        $this->nights = $nights;
    }

    /**
     * Handle the views.
     *
     * @param AddonTableBuilder $builder
     */
    public function handle(NightTableBuilder $builder)
    {
        $upcomingClasses = $this->nights
            ->newQuery()
            ->where("class_date", ">=", date("Y-m-d"))
            ->get();
        $pastClasses = $this->nights
            ->newQuery()
            ->where("class_date", "<", date("Y-m-d"))
            ->get();

        $builder->setViews(
            [
                'upcoming'    => [
                    "query" => UpcomingQuery::class,
                    'label'   => count($upcomingClasses) ?: 0,
                    "context" => "success",
                ],
                'past' => [
                    "query" => PastQuery::class,
                    "label" => count($pastClasses) ?: 0,
                    "context" => "success",
                    'order_by' => [
                        'class_date' => 'DESC',
                    ],
                ],
                
                'all' => [
                    "text"  => 'anomaly.module.addons::repository.all.name',
                    "context" => "success",
                ],
            ]
        );
    }
}
