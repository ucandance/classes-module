<?php namespace Finnito\ClassesModule\Night;

use Finnito\ClassesModule\Night\Contract\NightRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class NightRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightRepository extends EntryRepository implements NightRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var NightModel
     */
    protected $model;

    /**
     * Create a new NightRepository instance.
     *
     * @param NightModel $model
     */
    public function __construct(NightModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get the next class
     *
     * @return null|NightInterface
     */
    public function nextClass()
    {
        return $this->model
            ->where("class_date", ">=", date("Y-m-d"))
            ->orderBy("class_date", "ASC")
            ->first();
    }

    /**
     * Return future classes
     *
     * @return null|NightCollection
     */
    public function future()
    {
        return $this->model
            ->where("class_date", ">=", date("Y-m-d"))
            ->orderBy("class_date", "ASC")
            ->get();
    }

    /**
     * Find a class by date
     * in the format Y-m-d
     *
     * @param $class_date
     * @return null|NightInterface
     */
    public function findByDate($class_date)
    {
        return $this->model
            ->where("class_date", "=", $class_date)
            ->first();
    }
}
