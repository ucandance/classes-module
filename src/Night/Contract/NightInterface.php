<?php namespace Finnito\ClassesModule\Night\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class NightInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface NightInterface extends EntryInterface
{

    /**
     * Get the class names as an
     * HTML formatted string.
     *
     * @return null|string
     */
    public function getClassNames();
}
