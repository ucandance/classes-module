<?php namespace Finnito\ClassesModule\Night\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class NightRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface NightRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Get the next upcoming class
     *
     * @return null|NightInterface
     */
    public function nextClass();

    /**
     * Get all future classes
     *
     * @return null|NightCollection
     */
    public function future();

    /**
     * Find a class by a given date
     *
     * @param $class_date
     * @return null|NightInterface
     */
    public function findByDate($class_date);
}
