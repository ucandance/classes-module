<?php namespace Finnito\ClassesModule\Night\Command;

use Finnito\ClassesModule\Night\Contract\NightInterface;
use Anomaly\Streams\Platform\View\ViewTemplate;

/**
 * Class AddTemplateEditLink
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class AddTemplateEditLink
{

    /**
     * The NightRepositoryInterface
     *
     * @return null|string
     */
    protected $night;

    /**
     * Create a new AddTemplateEditLink instance.
     *
     * @return AddTemplateEditLink
     */
    public function __construct(NightInterface $night)
    {
        $this->night = $night;
    }

    /**
     * Handle the AddTemplateEditLink command
     *
     * @return null
     */
    public function handle(ViewTemplate $template)
    {
        $template->set('edit_link', "/admin/classes/edit/".$this->night->id);
        $template->set("edit_type", $this->night->class_date->format("Y-m-d") . " Class");
    }
}
