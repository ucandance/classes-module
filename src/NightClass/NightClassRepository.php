<?php namespace Finnito\ClassesModule\NightClass;

use Finnito\ClassesModule\NightClass\Contract\NightClassRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Class NightClassRepository
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassRepository extends EntryRepository implements NightClassRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var NightClassModel
     */
    protected $model;

    /**
     * Create a new NightClassRepository instance.
     *
     * @param NightClassModel $model
     */
    public function __construct(NightClassModel $model)
    {
        $this->model = $model;
    }
}
