<?php namespace Finnito\ClassesModule\NightClass\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class NightClassInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface NightClassInterface extends EntryInterface
{

}
