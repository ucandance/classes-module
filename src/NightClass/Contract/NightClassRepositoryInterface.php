<?php namespace Finnito\ClassesModule\NightClass\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Class NightClassRepositoryInterface
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
interface NightClassRepositoryInterface extends EntryRepositoryInterface
{

}
