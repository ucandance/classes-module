<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Entry\EntryRouter;

/**
 * Class NightClassRouter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassRouter extends EntryRouter
{

}
