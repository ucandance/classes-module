<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Entry\EntryObserver;

/**
 * Class NightClassObserver
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassObserver extends EntryObserver
{

}
