<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Model\Repeater\RepeaterNightClassesEntryModel;
use Finnito\ClassesModule\NightClass\Contract\NightClassInterface;

/**
 * Class NightClassModel
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassModel extends RepeaterNightClassesEntryModel implements NightClassInterface
{

    /**
     * Return the new repeater field type
     */
    public function newRepeaterFieldTypeFormBuilder()
    {
        return app(\Finnito\ClassesModule\NightClass\Support\RepeaterFieldType\FormBuilder::class);
    }
}
