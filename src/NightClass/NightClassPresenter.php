<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Class NightClassPresenter
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassPresenter extends EntryPresenter
{

}
