<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Class NightClassCollection
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassCollection extends EntryCollection
{

}
