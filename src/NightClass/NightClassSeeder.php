<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class NightClassSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        //
    }
}
