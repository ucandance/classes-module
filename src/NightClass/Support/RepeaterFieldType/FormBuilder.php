<?php namespace Finnito\ClassesModule\NightClass\Support\RepeaterFieldType;

class FormBuilder extends \Anomaly\Streams\Platform\Ui\Form\FormBuilder
{

    /**
     * The FormBuilder view options
     *
     * @return null|string
     */
    protected $options = [
        "input_view" => "finnito.module.classes::admin/classes/input_view",
        "form_view" => "finnito.module.classes::admin/classes/repeater_form",
    ];
}
