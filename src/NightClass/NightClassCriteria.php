<?php namespace Finnito\ClassesModule\NightClass;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

/**
 * Class NightClassCriteria
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassCriteria extends EntryCriteria
{

}
