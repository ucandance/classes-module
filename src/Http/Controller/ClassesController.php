<?php namespace Finnito\ClassesModule\Http\Controller;

use Finnito\ClassesModule\Night\Contract\NightRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\ClassesModule\Night\Command\AddTemplateEditLink;
use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;

/**
 * Class ClassesController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ClassesController extends PublicController
{

    /**
     * Display the next future class
     *
     * @param NightRepositoryInterface $nights
     * @param SettingRepositoryInterface $settings
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function next(NightRepositoryInterface $nights, SettingRepositoryInterface $settings)
    {
        $nextClass = $nights->nextClass();
        $futureClasses = $nights->future();

        // Breadcrumbs
        $this->breadcrumbs->add("Home", "/");
        $this->breadcrumbs->add('Classes', '/classes');

        // Add some info if exists
        if (!is_null($nextClass)) {
            // Meta Information
            $this->template->set("meta_title", "Next Class: {$nextClass->class_date->format('D jS M, Y')}");
            $this->template->set("meta_description", $settings->value("finnito.module.classes::meta_description"));
            $this->template->set("meta_image", $settings->value("finnito.module.classes::meta_image"));

            // Edt Links
            $this->template->set('edit_link', "/admin/classes/edit/".$nextClass->id);
            $this->template->set("edit_type", "Class on " . $nextClass->class_date);
        }

        // Render!
        return $this->view->make(
            "finnito.module.classes::class",
            [
                "classes" => $futureClasses,
                "class" => $nextClass
            ]
        );
    }

    /**
     * Display a single night of a given date
     *
     * @param NightRepositoryInterface $nights
     * @param SettingRepositoryInterface $settings
     * @param $class_date
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function single(NightRepositoryInterface $nights, SettingRepositoryInterface $settings, $class_date)
    {
        if (!$single_class = $nights->findByDate($class_date)) {
            abort(404);
        }

        $futureClasses = $nights->future();

        // Breadcrumbs
        $this->breadcrumbs->add("Home", "/");
        $this->breadcrumbs->add('Classes', '/classes');
        $this->breadcrumbs->add($single_class->class_date->format("Y-m-d"), "/classes/{$single_class->class_date->format("Y-m-d")}");

        // Meta Information
        $this->template->set("meta_title", "Class: {$single_class->class_date->format('D jS M, Y')}");
        $this->template->set("meta_description", $settings->value("finnito.module.classes::meta_description"));
        $this->template->set("meta_image", $settings->value("finnito.module.classes::meta_image"));

        // Template Edit links
        $this->template->set('edit_link', "/admin/classes/edit/".$single_class->id);
        $this->template->set("edit_type", "Class on " . $single_class->date);

        // Add some template edit links
        $this->dispatch(new AddTemplateEditLink($single_class));

        // Render
        return $this->view->make(
            "finnito.module.classes::class",
            [
                "classes" => $futureClasses,
                "class" => $single_class
            ]
        );
    }
}
