<?php namespace Finnito\ClassesModule\Http\Controller\Admin;

use Finnito\ClassesModule\Night\Contract\NightRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\ClassesModule\Night\Table\NightTableBuilder;
use Finnito\ClassesModule\Night\Form\NightFormBuilder;

/**
 * Class NightsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightsController extends AdminController
{

    /**
     * Display the table view of existing entries.
     *
     * @param NightTableBuilder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(NightTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Display an index of existing entries.
     *
     * @param NightTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calendar(NightRepositoryInterface $nights)
    {
        return $this->view->make(
            "finnito.module.classes::admin.index",
            [
                "nights" => $nights->all(),
            ]
        );
    }

    /**
     * Create a new entry.
     *
     * @param NightFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(NightFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param NightFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(NightFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
