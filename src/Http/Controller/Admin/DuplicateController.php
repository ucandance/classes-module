<?php namespace Finnito\ClassesModule\Http\Controller\Admin;

use Finnito\ClassesModule\Night\Contract\NightRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\ClassesModule\Night\NightModel;
use Carbon\CarbonInterval;

/**
 * Class DuplicateController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class DuplicateController extends AdminController
{

    /**
     * The NightRepositoryInterface
     *
     * @return null|string
     */
    private $nights;

    /**
     * Create a new DuplicateController intstance
     *
     * @return DuplicateController
     */
    public function __construct(NightRepositoryInterface $nights)
    {
        $this->nights = $nights;
    }

    /**
     * Do a basic duplicate + 1 week of the night
     *
     * @return Redirect
     */
    public function duplicate($id)
    {
        $this->doDuplicate($id);
        return back();
    }

    /**
     * Do a duplicate and edit of the night
     *
     * @return Redirect
     */
    public function duplicateEdit($id)
    {
        $entry = $this->doDuplicate($id);
        return redirect("/admin/classes/edit/{$entry->id}");
    }

    /**
     * The main duplicate function
     *
     * @return NightInterface
     */
    private function doDuplicate($id)
    {
        // Set the original night
        $originalNight = $this->nights->find($id);

        // Find the next appropriate entry ID
        $id = $this->nights->newQuery()->max("id") + 1;

        // Create the duplicated night
        $newNight = $this->nights->newInstance();
        $newNight->id = $id;
        $newNight->night_classes = $originalNight->night_classes->pluck("id");
        $newNight->class_date = $originalNight->class_date->add(CarbonInterval::weeks(1));
        $newNight->class_location = $originalNight->class_location;

        // Save it out
        $this->nights->save($newNight);

        return $newNight;
    }
}
