<?php namespace Finnito\ClassesModule\Http\Controller\Admin;

use Finnito\ClassesModule\ClassType\Contract\ClassTypeRepositoryInterface;
use Finnito\ClassesModule\ClassType\Table\ClassTypeTableBuilder;
use Finnito\ClassesModule\ClassType\Form\ClassTypeFormBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

/**
 * Class ClassTypesController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ClassTypesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ClassTypeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ClassTypeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ClassTypeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ClassTypeFormBuilder $form, ClassTypeRepositoryInterface $entries)
    {
        $e = $entries->newQuery()->where("year", "=", date("Y"))->get();
        var_dump($e->pluck("name"));
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ClassTypeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ClassTypeFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
