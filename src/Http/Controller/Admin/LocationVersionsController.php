<?php namesapce Finnito\ClassesModule\Http\Controller\Admin;

use Finnito\ClassesModule\Night\NightModel;

/**
 * Class LocationVersionsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationVersionsController extends \Anomaly\Streams\Platform\Http\Controller\VersionsController
{
    /**
     * The versionable model.
     *
     * @var string
     */
    protected $model = NightModel::class;
}
