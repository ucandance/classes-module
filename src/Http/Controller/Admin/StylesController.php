<?php namespace Finnito\ClassesModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\ClassesModule\Style\Table\StyleTableBuilder;
use Finnito\ClassesModule\Style\Form\StyleFormBuilder;

/**
 * Class StylesController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class StylesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param StyleTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(StyleTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param StyleFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(StyleFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param StyleFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(StyleFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
