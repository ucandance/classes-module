<?php namespace Finnito\ClassesModule\Http\Controller\Admin;

use Finnito\ClassesModule\NightClass\Table\NightClassTableBuilder;
use Finnito\ClassesModule\NightClass\Form\NightClassFormBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

/**
 * Class NightClassesController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class NightClassesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param NightClassTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(NightClassTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param NightClassFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(NightClassFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param NightClassFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(NightClassFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
