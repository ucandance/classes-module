<?php namespace Finnito\ClassesModule\Http\Controller\Admin;

use Finnito\ClassesModule\Location\Table\LocationTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\ClassesModule\Location\Form\LocationFormBuilder;
use Anomaly\Streams\Platform\Asset\Asset;

/**
 * Class LoctionsController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class LocationsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param LocationTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(LocationTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param LocationFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(LocationFormBuilder $form, Asset $asset)
    {
        $asset->add("theme.js", "finnito.module.classes::js/NightsFormHelper.js");
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param LocationFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(LocationFormBuilder $form, Asset $asset, $id)
    {
        $asset->add("theme.js", "finnito.module.classes::js/NightsFormHelper.js");
        return $form->render($id);
    }
}
