<?php namespace Finnito\ClassesModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class ClassesModule
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ClassesModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-calendar';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'nights' => [
            'buttons' => [
                'new_night',
            ],
        ],
        'locations' => [
            'buttons' => [
                'new_location',
            ],
        ],
        'styles' => [
            'buttons' => [
                'new_style',
            ],
        ],
    ];
}
