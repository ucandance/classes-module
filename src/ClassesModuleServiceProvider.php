<?php namespace Finnito\ClassesModule;

use Finnito\ClassesModule\NightClass\Contract\NightClassRepositoryInterface;
use Finnito\ClassesModule\Http\Controller\Admin\LocationVersionsController;
use Finnito\ClassesModule\ClassType\Contract\ClassTypeRepositoryInterface;
use Anomaly\Streams\Platform\Model\Classes\ClassesNightClassesEntryModel;
use Finnito\ClassesModule\Location\Contract\LocationRepositoryInterface;
use Anomaly\Streams\Platform\Model\Classes\ClassesClassTypesEntryModel;
use Anomaly\Streams\Platform\Model\Classes\ClassesLocationsEntryModel;
use Anomaly\Streams\Platform\Model\Classes\ClassesStylesEntryModel;
use Anomaly\Streams\Platform\Model\Classes\ClassesNightsEntryModel;
use Finnito\ClassesModule\Style\Contract\StyleRepositoryInterface;
use Finnito\ClassesModule\Night\Contract\NightRepositoryInterface;
use Finnito\ClassesModule\NightClass\NightClassRepository;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\ClassesModule\ClassType\ClassTypeRepository;
use Anomaly\Streams\Platform\Assignment\VersionRouter;
use Finnito\ClassesModule\Location\LocationRepository;
use Finnito\ClassesModule\NightClass\NightClassModel;
use Finnito\ClassesModule\ClassType\ClassTypeModel;
use Finnito\ClassesModule\Location\LocationModel;
use Finnito\ClassesModule\Style\StyleRepository;
use Finnito\ClassesModule\Night\NightRepository;
use Finnito\ClassesModule\Style\StyleModel;
use Finnito\ClassesModule\Night\NightModel;
use Illuminate\Routing\Router;

/**
 * Class ClassesModuleServiceProvider
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ClassesModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/classes/night_classes'           => 'Finnito\ClassesModule\Http\Controller\Admin\NightClassesController@index',
        'admin/classes/night_classes/create'    => 'Finnito\ClassesModule\Http\Controller\Admin\NightClassesController@create',
        'admin/classes/night_classes/edit/{id}' => 'Finnito\ClassesModule\Http\Controller\Admin\NightClassesController@edit',
        'admin/classes/styles'           => 'Finnito\ClassesModule\Http\Controller\Admin\StylesController@index',
        'admin/classes/styles/create'    => 'Finnito\ClassesModule\Http\Controller\Admin\StylesController@create',
        'admin/classes/styles/edit/{id}' => 'Finnito\ClassesModule\Http\Controller\Admin\StylesController@edit',
        'admin/classes/locations'           => 'Finnito\ClassesModule\Http\Controller\Admin\LocationsController@index',
        'admin/classes/locations/create'    => 'Finnito\ClassesModule\Http\Controller\Admin\LocationsController@create',
        'admin/classes/locations/edit/{id}' => 'Finnito\ClassesModule\Http\Controller\Admin\LocationsController@edit',
        'admin/classes/class_types'           => 'Finnito\ClassesModule\Http\Controller\Admin\ClassTypesController@index',
        'admin/classes/class_types/create'    => 'Finnito\ClassesModule\Http\Controller\Admin\ClassTypesController@create',
        'admin/classes/class_types/edit/{id}' => 'Finnito\ClassesModule\Http\Controller\Admin\ClassTypesController@edit',
        'admin/classes'           => 'Finnito\ClassesModule\Http\Controller\Admin\NightsController@index',
        'admin/classes/calendar'           => 'Finnito\ClassesModule\Http\Controller\Admin\NightsController@calendar',
        'admin/classes/create'    => 'Finnito\ClassesModule\Http\Controller\Admin\NightsController@create',
        'admin/classes/edit/{id}' => 'Finnito\ClassesModule\Http\Controller\Admin\NightsController@edit',

        // Duplicate +1 Week and Duplicate & Edit
        "admin/classes/duplicate/{id}" => "Finnito\ClassesModule\Http\Controller\Admin\DuplicateController@duplicate",
        "admin/classes/duplicate_edit/{id}" => "Finnito\ClassesModule\Http\Controller\Admin\DuplicateController@duplicateEdit",

        // Public routes
        "classes" => "Finnito\ClassesModule\Http\Controller\ClassesController@next",
        "classes/{date}" => "Finnito\ClassesModule\Http\Controller\ClassesController@single",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\ClassesModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\ClassesModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\ClassesModule\Event\ExampleEvent::class => [
        //    Finnito\ClassesModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\ClassesModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        ClassesNightClassesEntryModel::class => NightClassModel::class,
        ClassesStylesEntryModel::class => StyleModel::class,
        ClassesLocationsEntryModel::class => LocationModel::class,
        ClassesClassTypesEntryModel::class => ClassTypeModel::class,
        ClassesNightsEntryModel::class => NightModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        NightClassRepositoryInterface::class => NightClassRepository::class,
        StyleRepositoryInterface::class => StyleRepository::class,
        LocationRepositoryInterface::class => LocationRepository::class,
        ClassTypeRepositoryInterface::class => ClassTypeRepository::class,
        NightRepositoryInterface::class => NightRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router, VersionRouter $versions)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }
}
