<?php namespace Finnito\ClassesModule;

use Finnito\ClassesModule\ClassType\ClassTypeSeeder;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class ClassesModuleSeeder
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class ClassesModuleSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        // $this->call(ClassTypeSeeder::class);
    }
}
